# MDboy

Portage simplifié de l'arduboy pour réaliser des ateliers avec les enfants

## Getting started

Installer la dernière version d'arduino : https://www.arduino.cc/en/software
Installer le package homemade arduboy : https://github.com/MrBlinky/Arduboy-homemade-package
Une fois que c'est fait, dans le logiciel arduino, aller dans "outils", puis 
- dans "Type de carte" sélectionner "Homemade Arduboy"
- dans "Based On" sélectionner Sparkfun Pro micro 5v - Standard Wiring
- dans Affichage : SH1106 
