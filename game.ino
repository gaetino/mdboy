#include <Arduboy2.h>
Arduboy2 arduboy;


const unsigned char PROGMEM heart[] = {
8, 8, 
0x30, 0x48, 0x90, 0x48, 0x30, 0x00, 0x00, 0x00, };




//**********************************************************************************
//                             SPRITES
//**********************************************************************************

const unsigned char PROGMEM player[] = {
16, 8, 
0x00, 0x00, 0x14, 0x14, 0x3E, 0x3E, 0x7F, 0x7F, 0x3E, 0x2A, 0x22, 0x14, 0x08, 0x08, 0x08, 0x08, };


const unsigned char PROGMEM balle[] = {
8, 8, 
0x18, 0x18, 0x18, 0x18, 0x00, 0x00, 0x00, 0x00, };
byte haut_tir=4;
byte bas_tir=5;
byte largeur_tir=4;
byte vitesse_tir=4;

const unsigned char PROGMEM balle_hhh[] = {
8, 8, 
0x0C, 0x0F, 0x03, 0x00, 0x00, 0x00, 0x00, 0x00, };
const unsigned char PROGMEM balle_h[] = {
8, 8, 
0x18, 0x18, 0x0C, 0x0C, 0x00, 0x00, 0x00, 0x00, };
const unsigned char PROGMEM balle_b[] = {
8, 8, 
0x18, 0x18, 0x30, 0x30, 0x00, 0x00, 0x00, 0x00, };
const unsigned char PROGMEM balle_bb[] = {
8, 8, 
0x30, 0x70, 0xE0, 0xC0, 0x00, 0x00, 0x00, 0x00, };
const unsigned char PROGMEM balle_bbb[] = {
8, 8, 
0x30, 0xF0, 0xC0, 0x00, 0x00, 0x00, 0x00, 0x00, };


/*
const byte PROGMEM ennemy1[] = {
8, 8, 
0x81, 0x81, 0x42, 0x42, 0x3E, 0x3C, 0x3C, 0x18, };

const byte PROGMEM ennemy2[] = {
8, 8, 
0x08, 0x08, 0x1C, 0x2A, 0x49, 0x49, 0x49, 0x3E, };

const byte PROGMEM obstacle1[] = {
8, 16, 
0xFF, 0x85, 0x49, 0x31, 0x31, 0x49, 0x85, 0xFF, 0xFF, 0xA1, 0x92, 0x8C, 0x8C, 0x92, 0xA1, 0xFF, };*/
const unsigned char PROGMEM ennemy1[] = {
16, 8, 
0x00, 0x00, 0x00, 0x28, 0x54, 0x44, 0x44, 0x44, 0x44, 0x54, 0x28, 0x54, 0x28, 0x10, 0x00, 0x00, };

const unsigned char PROGMEM ennemy2[] = {
16, 8, 
0x00, 0x00, 0x00, 0x00, 0x41, 0x63, 0x55, 0x7F, 0x41, 0x22, 0x14, 0x08, 0x00, 0x00, 0x00, 0x00, };


const unsigned char PROGMEM ennemy3[] = {
16, 8, 
0x00, 0x00, 0x00, 0x00, 0x39, 0x47, 0x54, 0x54, 0x47, 0x39, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, };


//**********************************************************************************
//                             CHEMINS DES ENNEMIS
//**********************************************************************************
//----------------------------
// Toujours commencer la première position à "128,ordonnée"
// Toujours terminer la dernière position avec une abscisse à 0
// les abscisses vont de 0 à 128 (gauche à droite), et ordonnées de 0 à 64 (haut en bas).
// Abscisse à 130 : On fait une boucle en revenant y positions en arrière. y est l'ordonnée.
// Abscisse à 140 : Le vaisseau ennemi se dirige toujours vers le joueur. La valeur y détermine la vitesse. A 0, le vaisseau reste à sa place en suivant le joueur. 
//                  Après avoir mis une abscisse à 140, la pattern n'évolue plus. Il ne sert à rien de rajouter un autre chemin derrière.



const byte pattern1[]={128,10, 
                 10,10, 
                 10,32, 
                 80,32, 
                 0,32
                 } ;
const byte pattern2[]={128,40, 
                 10,40, 
                 10,10, 
                 0,10 
                 }; 
const byte pattern3[]={128,10, 
                 100,10, 
                 100,54, 
                 130,2 
                 };
const byte pattern4[]={128,5, 
                 115,5,
                 140,1 
                 };
const byte pattern5[]={128,0, 
                 0,0
                 }; 
const byte pattern6[]={128,48, 
                 0,48
                 }; 

byte *pointeur[] ={&pattern1[0], &pattern2[0], &pattern3[0], &pattern4[0], &pattern5[0], &pattern6[0]};
//**********************************************************************************
//**********************************************************************************




int playerx = 5;
int playery = 10;

long beg;



struct Tir
{
  byte x;
  byte y;
  byte fired;
};
Tir tir[8];

struct Star
{
  byte x;
  byte y;
};
Star stars[10];

struct Ennemy
{
  byte x;
  float y;
  byte pattern_pos;
  byte life;
  byte *patternnum;
  byte vaisseau_num;
  byte type;
  byte tirx;
  byte tiry;
};
Ennemy ennemy[10];

struct Ennemydesc
{
  byte *spritenum;
  byte life;
  byte type;
  byte haut;
  byte bas;
};
Ennemydesc ennemydesc[6];

//**********************************************************************************
//                             APPARITION DES ENNEMIS
//                                chiffre 1 : nombre de secondes avant l'apparition du vaisseau
//                                chiffre 2 : numéro du vaisseau décrit par Ennemydesc
//                                chiffre 3 : le numéro du chemin que va emprunter le vaisseau décrit par pointeur
//**********************************************************************************  

const byte PROGMEM apparitions[]={
  3,0,3,
  4,0,1,
  5,0,0,
  6,1,2,
  7,2,4,
  8,2,5,
  13,1,0,
  13,0,1,
  14,1,3,
};

//**********************************************************************************
//********************************************************************************** 

byte ennemycount=0;
byte ennemymax=sizeof(apparitions)/3;

byte player_life = 3;
byte invincible=0;

byte game_state = 1;

long begin_time=0;
int framecount=0;

void setup() {
  // put your setup code here, to run once:
  //Serial.begin(9600);
  arduboy.begin();
  arduboy.clear();
  game_init();
}

void loop() {
  // put your main code here, to run repeatedly: 
  if(game_state==1)
  {
    commence_boucle();
    
//**********************************************************************************
//                             COMPOSANTS DU JEU
//**********************************************************************************
    affiche_ui();
    affiche_etoiles();
    deplace_ennemis();
    controle_sprite(player);
    activer_tir(balle,player);
    //detecte_collision();
//**********************************************************************************
//**********************************************************************************

    finit_boucle();
    detecte_collision();
  }
  else
  {
    /*commence_boucle();
    arduboy.println("gameover");
    finit_boucle();*/
    commence_boucle();
    arduboy.clear();
    //arduboy.print("gameover");
    delay(5);
    finit_boucle();
  }
}

void game_init()
{
  byte en=0;
  for(int i = 0; i<10; i++)
  {
    stars[i].x=129;
  }
  for(int i = 0; i<sizeof(ennemy); i++)
  {
    ennemy[i].x=0;
    ennemy[i].y=0;
    ennemy[i].pattern_pos=0;
    ennemy[i].tirx=0;
    ennemy[i].tiry=0;
    ennemy[i].type=0;
  }

    

//**********************************************************************************
//                             DESCRIPTION VAISSEAUX ENNEMIS
//                                type=0 : normal
//                                type=1 : l'ennemi tire
//**********************************************************************************
 /* ennemydesc[en].spritenum = &ennemy1[0];
  ennemydesc[en].life = 4;
  ennemydesc[en].type = 0;
  ennemydesc[en].haut = 0;
  ennemydesc[en].bas = 8;
  en++;

  ennemydesc[en].spritenum = &ennemy2[0];
  ennemydesc[en].life = 4;
  ennemydesc[en].type = 1;
  ennemydesc[en].haut = 0;
  ennemydesc[en].bas = 8;
  en++;

  ennemydesc[en].spritenum = &obstacle1[0];
  ennemydesc[en].life = 10;
  ennemydesc[en].type = 0;
  ennemydesc[en].haut = 0;
  ennemydesc[en].bas = 16;
  en++;*/

  ennemydesc[en].spritenum = &ennemy1[0];
  ennemydesc[en].life = 1;
  ennemydesc[en].type = 0;
  ennemydesc[en].haut = 0;
  ennemydesc[en].bas = 8;
  en++;

  ennemydesc[en].spritenum = &ennemy2[0];
  ennemydesc[en].life = 3;
  ennemydesc[en].type = 0;
  ennemydesc[en].haut = 0;
  ennemydesc[en].bas = 8;
  en++;

  ennemydesc[en].spritenum = &ennemy3[0];
  ennemydesc[en].life = 5;
  ennemydesc[en].type = 0;
  ennemydesc[en].haut = 0;
  ennemydesc[en].bas = 8;
  en++;
  
//**********************************************************************************
//**********************************************************************************


  ennemycount=0;
  

  begin_time = millis();
}

void commence_boucle()
{
  beg = millis();
  arduboy.clear();
  arduboy.pollButtons();
  if (arduboy.justPressed(A_BUTTON) && game_state==0) {
    //game_init();
    game_state=1;
  }
}

void finit_boucle()
{
  arduboy.display();
  delay(30 - millis()+beg);
  if(invincible>0) invincible--;
  framecount++;
}

void affiche_ui()
{
  for(int i = 0; i<player_life; i++)
  {
    Sprites::drawOverwrite(i*7, 55, heart, 0);
  }
}

void deplace_ennemis()
{
  //if(millis()>(apparitions[ennemycount].temps*1000+begin_time) && ennemycount<ennemymax)
  if(millis()>(pgm_read_byte(apparitions+(ennemycount*3))*1000+begin_time) && ennemycount<ennemymax)
  
  {
    for(int i = 0; i<10; i++)
    {
      if(ennemy[i].x==0)
      {
        ennemy[i].pattern_pos=0;
        ennemy[i].patternnum = pointeur[pgm_read_byte(apparitions+(ennemycount*3)+2)];
        ennemy[i].x=*ennemy[i].patternnum;
        ennemy[i].y=*(ennemy[i].patternnum+1);
        ennemy[i].life=ennemydesc[pgm_read_byte(apparitions+(ennemycount*3)+1)].life;
        ennemy[i].type=ennemydesc[pgm_read_byte(apparitions+(ennemycount*3)+1)].type;
        ennemy[i].vaisseau_num=pgm_read_byte(apparitions+(ennemycount*3)+1);
        ennemycount++;
        return;
      }      
    }      
  }
  for(int i = 0; i<10; i++)
  {
    byte px= *(ennemy[i].patternnum + ennemy[i].pattern_pos*2);
    byte py= *(ennemy[i].patternnum + ennemy[i].pattern_pos*2+1);
    if(ennemy[i].x>0 && px<130)
    {
      if(ennemy[i].x>px) ennemy[i].x--;
      if(ennemy[i].x<px) ennemy[i].x++;
      if(ennemy[i].y>py) ennemy[i].y-=1.0;
      if(ennemy[i].y<py) ennemy[i].y+=1.0;
      if(ennemy[i].x==px && ennemy[i].y>(py-1) && ennemy[i].y<(py+1)) ennemy[i].pattern_pos++;
      Sprites::drawOverwrite((int)ennemy[i].x, (int)ennemy[i].y, ennemydesc[ennemy[i].vaisseau_num].spritenum, 0);
    }
    if(px==130)
    {
      ennemy[i].pattern_pos-=py;
    }
    if(ennemy[i].x>0 && px==140)
    {
      float dx = ennemy[i].x-playerx;
      float dy = ennemy[i].y-playery;
      if(dx<0) ennemy[i].x+=py;
      if(dx>0) ennemy[i].x-=py;
      if(dy<0 && dx>30 && py>0) ennemy[i].y-=dy/dx;
      if(dy>0 && dx>30 && py>0) ennemy[i].y+=dy/dx;
      if(dy<0 && (dx<=30||py==0)) ennemy[i].y+=1;
      if(dy>0 && (dx<=30||py==0)) ennemy[i].y-=1;
      Sprites::drawOverwrite((int)ennemy[i].x, (int)ennemy[i].y, ennemydesc[ennemy[i].vaisseau_num].spritenum, 0);
    }
    if(ennemy[i].type==1)
    {
      if(ennemy[i].tirx==0) 
      {
        ennemy[i].tirx=ennemy[i].x;
        ennemy[i].tiry=(byte)ennemy[i].y;
      }
      else
      {
        ennemy[i].tirx-=2;
        Sprites::drawOverwrite(ennemy[i].tirx, ennemy[i].tiry, balle, 0);
      }
    }
  }
}

void affiche_etoiles()
{
  /*if(millis()>0 && stars[0].x==129) {stars[0].x=128; stars[0].y=random(0,64);}
  if(millis()>2450 && stars[1].x==129) {stars[1].x=128; stars[1].y=random(0,64);}
  if(millis()>2600 && stars[2].x==129) {stars[2].x=128; stars[2].y=random(0,64);}
  if(millis()>3000 && stars[2].x==129) {stars[3].x=128; stars[3].y=random(0,64);}*/

  
  for(int i = 0; i<10; i++)
  {
    if(random(0,200)<3 && stars[i].x==129) {stars[i].x=128; stars[i].y=random(0,64);}
    if(stars[i].x<129 && stars[i].x>0) stars[i].x--;
    if(stars[i].x==0 && random(0,30)<3) {stars[i].x=128; stars[i].y=random(0,64);}
    if(stars[i].x!=0)arduboy.drawFastHLine(stars[i].x, stars[i].y, 1, WHITE);
  }
  
}

void detecte_collision()
{
  for(int j = 0; j<10; j++)
  {
    //Serial.println(ennemy[j].x);
    if(ennemy[j].x!=0)
    {
      //arduboy.println(*ennemydesc[ennemy[j].vaisseau_num].spritenum);
      //if(ennemy[j].tirx!=0 && (playerx+16)>ennemy[j].tirx) {arduboy.println("touch");}
      if(invincible==0)
      {
        // joueur touché par un tir
        if(ennemy[j].tirx!=0 && (playerx+16)>=ennemy[j].tirx && playerx<=ennemy[j].tirx && (ennemy[j].tiry+haut_tir)<=(playery+8) && (ennemy[j].tiry+bas_tir)>=playery) 
        {
          player_life--;
          invincible=60;
          if(player_life<=0)
          {
            game_init();
            game_state=0;
            player_life=3;
          }
        }
        //joueur touché par ennemi
        if((playerx+16)>=ennemy[j].x && playerx<ennemy[j].x && (playery+8)>=(byte)(ennemy[j].y+ ennemydesc[ennemy[j].vaisseau_num].haut) && playery<=(byte)(ennemy[j].y+ ennemydesc[ennemy[j].vaisseau_num].bas)) 
        {
          ennemy[j].x=0;
          player_life--;
          invincible=60;
          if(player_life<=0)
          {
            game_init();
            game_state=0;
            player_life=3;
          }
        }
      }
      //ennemi touché par tir du joueur
      for(int i = 0; i<8; i++)
      {
        if (tir[i].fired)
        {
          //if(tir[i].x>ennemy[j].x && tir[i].x<(ennemy[j].x+8) && tir[i].y>(ennemy[j].y-4) && tir[i].y<(ennemy[j].y+4)) 
          if(tir[i].x+largeur_tir>=ennemy[j].x && tir[i].x<=(ennemy[j].x+ 8) && (tir[i].y+bas_tir)>=(ennemy[j].y+ ennemydesc[ennemy[j].vaisseau_num].haut) && (tir[i].y+haut_tir)<=(ennemy[j].y+ ennemydesc[ennemy[j].vaisseau_num].bas))
          {
            tir[i].fired=0;
            ennemy[j].life--;
            if(ennemy[j].life<1) ennemy[j].x=0;
          }
        }
      }
    }
  }
}

void controle_sprite(unsigned char pl[])
{
  if(invincible%10<5) Sprites::drawOverwrite(playerx, playery, pl, 0);
  if (arduboy.pressed(LEFT_BUTTON)) {
    if(playerx>0) playerx = playerx - 1;
  }
  if (arduboy.pressed(RIGHT_BUTTON)) {
      if(playerx<(128-pl[0])) playerx = playerx + 1;
  }
  if (arduboy.pressed(UP_BUTTON)) {
      if(playery>0) playery = playery - 1;
  }
  if (arduboy.pressed(DOWN_BUTTON)) {
      if(playery<64-(pl[1])) playery = playery + 1;
  }
}

void activer_tir(unsigned char tr[], unsigned char pl[])
{
  if (arduboy.justPressed(A_BUTTON)) {
    for(int i = 0; i<8; i++)
    {
      if (tir[i].fired==0)
      {
        tir[i].fired=1;
        tir[i].x=playerx+pl[0];
        tir[i].y=playery;
        return;
      }
    }
  }
  for(int i = 0; i<8; i++)
  {
    if(tir[i].fired)
    {
      Sprites::drawOverwrite(tir[i].x, tir[i].y, tr, 0);
      tir[i].x+=2;
      if(tir[i].x>127) tir[i].fired=0;
    }
  }
}

